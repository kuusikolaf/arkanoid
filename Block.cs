﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.Graphics;
using SFML.System;

namespace Arkanoid
{
    class Block : GameObject
    {
        public Block(Vector2f position, Vector2f blockSize)
        {
            Shape = new RectangleShape(blockSize);
            Shape.FillColor = new Color(255, 69, 0);
            Position = position;
        }
    }
}
