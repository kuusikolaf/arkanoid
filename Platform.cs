﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Arkanoid
{
    class Platform : GameObject
    {
        const float PLATFORM_SPEED = 180.0f;

        public Platform(Vector2f position, Vector2f platformSize)
        {
            Shape = new RectangleShape(platformSize);
            Shape.FillColor = new Color(255, 165, 0);
            Position = position;
        }

        public override void Update(float deltaTime)
        {
            Vector2f pos = Position;

            if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
            {
                pos.X -= PLATFORM_SPEED * deltaTime;
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
            {
                pos.X += PLATFORM_SPEED * deltaTime;
            }
            if (pos.X <= 0)
            {
                pos.X = 0.0f;
            }
            if (pos.X >= (World.ScreenSize.X - Shape.GetGlobalBounds().Width))
            {
                pos.X = World.ScreenSize.X - Shape.GetGlobalBounds().Width;
            }
            Position = pos;
        }
    }
}
