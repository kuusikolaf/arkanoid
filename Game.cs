﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arkanoid
{
    public class Game
    {
        public Vector2f ScreenSize
        {
            get;
        }
        public Game(int windowWidth, int windowHeight)
        {
            Vector2f screenSize = new Vector2f(windowWidth, windowHeight);
            ScreenSize = screenSize;
        }
    }
}
