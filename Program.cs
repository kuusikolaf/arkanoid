﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace Arkanoid
{
    class Program
    {
        const uint FRAMERATE = 60;
        const int WINDOWHEIGHT = 800;
        const int WINDOWWIDTH = 1000;

        public static Game World;

        static void Main(string[] args)
        {
            VideoMode videoConf = new VideoMode(WINDOWWIDTH, WINDOWHEIGHT);
            World = new Game((int)videoConf.Width, (int)videoConf.Height);
            
            RenderWindow window = new RenderWindow(videoConf, "Arkanoid", Styles.Close);
            window.Closed += Window_Closed;
            window.SetFramerateLimit(FRAMERATE);

            List<GameObject> gameObjects = new List<GameObject>();

            CreateBlocks(gameObjects, 4, 7, new Vector2f(20.0f, 20.0f), new Vector2f(120.0f, 50.0f));
            Ball ball = new Ball(new Vector2f(480.0f, 740.0f));
            gameObjects.Add(ball);
            Platform platform = new Platform(new Vector2f(460.0f, 780.0f), new Vector2f(80.0f, 20.0f));
            gameObjects.Add(platform);

            while (window.IsOpen)
            {
                window.DispatchEvents();

                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].Update(1.0f / FRAMERATE);
                }


                window.Clear(Color.Black);

                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].Draw(window);
                }

                window.Display();
            }
        }

        //////////////////////////////////////////////////////////////////////////////////

        // Method for positioning blocks into table form
        static void CreateBlocks(List<GameObject> objects, int numRows, int numCols,
                                 Vector2f margin, Vector2f blockSize)
        {
            Vector2f pos = margin;
            for (int y = 0; y < numRows; y++)
            {
                for (int x = 0; x < numCols; x++)
                {
                    objects.Add(new Block(pos, blockSize));
                    pos.X += blockSize.X + margin.X;
                }
                pos.X = margin.X;
                pos.Y += blockSize.Y + margin.Y;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////

        private static void Window_Closed(object sender, EventArgs e)
        {
            RenderWindow myWindow = (RenderWindow)sender;
            myWindow.Close();
        }
    }
}
