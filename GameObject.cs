﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.Graphics;
using SFML.System;

namespace Arkanoid
{
    public class GameObject
    {
        public Game World = Program.World;
        public Shape Shape = null;

        public Vector2f Position
        {
            get { return this.Shape.Position; }
            set { Shape.Position = value; }
        }

        public virtual void Update(float deltaTime)
        {

        }

        public virtual void Draw(RenderWindow window)
        {
            window.Draw(Shape);
        }
    }
}
