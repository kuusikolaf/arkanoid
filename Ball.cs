﻿using System;
using System.Collections.Generic;
using System.Text;
using SFML.Graphics;
using SFML.System;

namespace Arkanoid
{
    class Ball : GameObject
    {
        public Ball(Vector2f position)
        {
            Shape = new CircleShape(20.0f);
            Shape.FillColor = new Color(255, 215, 0);
            Position = position;
        }
    }
}
